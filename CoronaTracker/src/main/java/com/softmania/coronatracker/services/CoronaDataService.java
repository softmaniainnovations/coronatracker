/**
 * 
 */
package com.softmania.coronatracker.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.http.HttpRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.softmania.coronatracker.model.LocationStats;

/**
 * @author Sushil
 *
 */
@Service
public class CoronaDataService {
	private static String URL_Confirmed = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";
	private static String URL_Recovered = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv";
	private static String URL_Deaths = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv";
	private List<LocationStats> lstStats = new ArrayList<LocationStats>();
	private int totalcases = 0;

	public List<LocationStats> getLstStats() {
		return lstStats;
	}

	public int getTotalcases() {
		return totalcases;
	}



	@PostConstruct
	@Scheduled(cron = "* 35 * * * *")
	public void fetchData() {
		List<LocationStats> stats = new ArrayList<LocationStats>();
		totalcases = 0;
		try {
			// Confirmed Cases
			URL urlConfirmed = new URL(URL_Confirmed);
			HttpURLConnection conConfirmed = (HttpURLConnection) urlConfirmed.openConnection();
			conConfirmed.setRequestMethod("GET");

			BufferedReader in = new BufferedReader(new InputStreamReader(conConfirmed.getInputStream()));

			Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
			for (CSVRecord record : records) {
				LocationStats lstats = new LocationStats();
				lstats.setState(record.get("Province/State"));
				lstats.setCountry(record.get("Country/Region"));
				String strCases = record.get(record.size()-1);
				int cases = 0;
				if (strCases != null && !strCases.isEmpty()) {
					cases = Integer.parseInt(strCases);
				}
				
				String strPrevDaycases = record.get(record.size()-2);
				int prevDaycases = 0;
				if (strPrevDaycases != null && !strPrevDaycases.isEmpty()) {
					prevDaycases = Integer.parseInt(strPrevDaycases);
				}
				lstats.setTotalCases(cases);
				lstats.setDiffFromPrevDay(cases - prevDaycases);
			    stats.add(lstats);
			    totalcases += cases;
			}
			in.close();
			
			// Recovered Cases
			URL urlRecovered = new URL(URL_Recovered);
			HttpURLConnection conRecovered = (HttpURLConnection) urlRecovered.openConnection();
			conRecovered.setRequestMethod("GET");

			in = new BufferedReader(new InputStreamReader(conRecovered.getInputStream()));

			records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
			int i = 0;
			for (CSVRecord record : records) {
				LocationStats lstats = stats.get(i);
				String strRecoveredCases = record.get(record.size()-1);
				int recoveredCases = 0;
				if (strRecoveredCases != null && !strRecoveredCases.isEmpty()) {
					recoveredCases = Integer.parseInt(strRecoveredCases);
				}
				lstats.setTotalRecovered(recoveredCases);
				i++;
			}
			in.close();
			
			// Death Cases
			URL urlDeath = new URL(URL_Deaths);
			HttpURLConnection conDeath = (HttpURLConnection) urlDeath.openConnection();
			conDeath.setRequestMethod("GET");

			in = new BufferedReader(new InputStreamReader(conDeath.getInputStream()));

			records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
			i = 0;
			for (CSVRecord record : records) {
				LocationStats lstats = stats.get(i);
				String strDeaths = record.get(record.size()-1);
				int deathCases = 0;
				if (strDeaths != null && !strDeaths.isEmpty()) {
					deathCases = Integer.parseInt(strDeaths);
				}
				lstats.setTotalDeaths(deathCases);
				i++;
			}
			in.close();
			
			this.lstStats = stats;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
