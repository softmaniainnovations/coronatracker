package com.softmania.coronatracker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.softmania.coronatracker.model.LocationStats;
import com.softmania.coronatracker.services.CoronaDataService;

@Controller
public class HomeController {
	
	@Autowired
	CoronaDataService svc;

	@GetMapping("/")
	public String home(Model model) {
		List<LocationStats> lst = svc.getLstStats();
		int totalNewCases = lst.stream().mapToInt(stat -> stat.getDiffFromPrevDay()).sum();
		int totalRecovered = lst.stream().mapToInt(stat -> stat.getTotalRecovered()).sum();
		int totalDeaths = lst.stream().mapToInt(stat -> stat.getTotalDeaths()).sum();
		model.addAttribute("totalCases",svc.getTotalcases());
		model.addAttribute("totalNewCases",totalNewCases);
		model.addAttribute("totalRecovered",totalRecovered);
		model.addAttribute("totalDeaths",totalDeaths);
		model.addAttribute("locationStats", lst);
		return "home";
	}
}
